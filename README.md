# Level-Blocking-Remake-SP22

Level Blocking Remake Exercise for the SP22 section of BL-MSCH-G300. 26 January 2022.


## Implementation
Built using Unity2020.3.26f1

Reference for Level Block from noclip.website


## References

Original Level: Tutorial from *Katamari Damacy* 

## Future Development
None

## Created by
Gwen Hall
